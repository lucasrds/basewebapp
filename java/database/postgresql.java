package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class postgresql {

	public postgresql() {
	}

	public Connection connect() throws SQLException {
		String url = "jdbc:postgresql://localhost:5432/teste";
		Properties props = new Properties();
		props.setProperty("user", "postgres");
		props.setProperty("password", "123");
		props.setProperty("ssl", "true");
		return DriverManager.getConnection(url, props);
	}
}
