package Product;

public class ProductBuilder {
	private Product product;

	public ProductBuilder(String name, double price) {
		this.product = new Product(name, price);
	}

	public ProductBuilder description(String description) {
		this.product.description = description;
		return this;
	}

	public Product build() {
		return product;
	}
}
