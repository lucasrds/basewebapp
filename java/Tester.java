import Product.Product;
import Product.ProductBuilder;

public class Tester {
	public static void main(String... args){
		Product product = new ProductBuilder("Lactose", 5.20f).build();
		System.out.println(product.toString());
		product = new ProductBuilder("Queijo",10.50f)
				.description("Queijo saturado")
				.build();
		System.out.println(product.toString());
	}
}
